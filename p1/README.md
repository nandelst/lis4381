# LIS4381 - Mobile Web Application Development

## Douglas Nandelstadt

### Project 1 Requirements:

*One part*:

1. Completed My Business Card! app with:

	a) Launcher icon in both activity screens
	
	b) Background colors in both activity screens
	
	c) Borders added around image and button
	
	d) Text shadow added to button
	
#### README.md file should include the following items:

* Screenshot of running application's first and second user interfaces
* Screenshots of skill sets 7, 8, and 9

#### Assignment Screenshots:

*Screenshot of running application's first and second user interfaces*:

| App First Interface | App Second Interface |
| :---: | :---: |
| ![Screenshot of the app's first interface](img/screenshot1.PNG) | ![Screenshot of the app's second interface](img/screenshot2.PNG) |

*Screenshot of Skill Sets*:

| Skill Set 7 | Skill Set 8 | Skill Set 9 |
| :---: | :---: | :---: |
| ![Screenshot of Skill Set 7 running](img/RandomArray.PNG) | ![Screenshot of Skill Set 8 running](img/LargestThreeNumbers.PNG) | ![Screenshot of Skill Set 9 running](img/ArrayRuntime.PNG) |

#### Assignment Links:

*Bitbucket Repository*:
[P1 Bitbucket Repository Link](https://bitbucket.org/nandelst/lis4381/ "My LIS4381 Bitbucket Repository")