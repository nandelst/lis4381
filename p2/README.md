# LIS4381 - Mobile Web Application Development

## Douglas Nandelstadt

### Project 2 Requirements:

*Two parts*:

1. Completed assignment 5 directory and files
2. RSS feed page completed

	
#### README.md file should include the following items:

* Course title, my name, and the assignment requirements, as per A1
* Screenshot of my local LIS4381 Web Application carousel
* Screenshot of project 2's index.php,  edit_petstore.php (failed and passed validation), and delete_petstore.php (delete prompt and deletion)
* Screenshot of the RSS feed
* Link to the local LIS4381 Web Application

#### Assignment Screenshots:

*Screenshot of the local LIS4381 Web Application carousel and project 2's index.php*:

| Local LIS4381 Web Application carousel | Project 2's index.php |
| :---: | :---: |
| ![Screenshot of local LIS4381 Web Application carousel](img/homepage.PNG) | ![Screenshot of project 2's index.php](img/index.PNG) |

*Screenshots of edit_petstore's update page and failing and passing valdiation*:

| Update page | Failing validation | Passing validation
| :---: | :---: | :---: |
| ![Screenshot of edit_petstore.php's update page](img/editpetstore.PNG) | ![Screenshot of edit_petstore.php failing validation](img/failededitpetstore.PNG) | ![Screenshot of edit_petstore.php passing validation](img/passededitpetstore.PNG) |

*Screenshots of delete prompt and deletion*:

| Delete Prompt | Deleted |
| :---: | :---: |
| ![Screenshot of delete prompt](img/deleteprompt.PNG) | ![Screenshot of deletion](img/deleted.PNG) |

*Screenshot of RSS feed page*:

![RSS feed page](img/rss.PNG)

#### Assignment Links:

*Local LIS4381 Web Application*:
[Link to local LIS4381 Web Application](http://localhost/repos/lis4381/ "My LIS4381 localhost Web Application")