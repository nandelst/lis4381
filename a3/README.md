# LIS4381 - Mobile Web Application Development

## Douglas Nandelstadt

### Assignment 3 Requirements:

*Three Parts:*

1. Completed ERD with MySQL Workbench
2. Forward engineered database with 10 records in each table
3. Completed My Event Android app with:

	a) Launcher icon
	
	b) Colors added to activity controls
	
	c) Borders added around image and button
	
	d) Text shadow added to button
	
#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Screenshots of skill sets 4, 5, and 6
* Links to the following files:
	* a3.mwb
	* a3.sql

#### Assignment Screenshots:

*Screenshot of ERD*:

![Screenshot of ERD](img/erd.PNG)

*Screenshot of running application's first and second user interfaces*:

| App First Interface | App Second Interface |
| :---: | :---: |
| ![Screenshot of the app's first interface](img/screenshot1.PNG) | ![Screenshot of the app's second interface](img/screenshot2.PNG) |

*Screenshot of Skill Sets*:

| Skill Set 4 | Skill Set 5 | Skill Set 6 |
| :---: | :---: | :---: |
| ![Screenshot of Skill Set 4 running](img/DecisionStructures.PNG) | ![Screenshot of Skill Set 5 running](img/RandomArray.PNG) | ![Screenshot of Skill Set 6 running](img/Methods.PNG) |

#### Assignment Links:

*Link to a3.mwb*:
[a3.mwb](docs/a3.mwb "Link to a3.mwb")

*Link to a3.sql*:
[a3.sql](docs/a3.sql "Link to a3.sql")