# LIS4381 - Mobile Web Application Development

## Douglas Nandelstadt

### Assignment 4 Requirements:

*One part*:

1. Completed LIS4381 Web application with:

	a) Main page with three carousel slides that contain text and images
	
	b) A1, A2, A3, and P1 links working with content in each index.php file
	
	c) Assignment 4 index.php completed with working data validation
	
	d) A favicon placed in each assignment's main directory
	
	
#### README.md file should include the following items:

* Course title, my name, and the assignment requirements, as per A1
* Screenshot of the Web Application's Main Page, Failed Validation, and Passed Validation
* Screenshots of skill sets 10, 11, and 12
* Link to the local LIS4381 Web Application

#### Assignment Screenshots:

*Screenshot of Web Application's Main Page, Failed Validation, and Passed Validation*:

| Main Page | Failed Validation | Passed Validation |
| :---: | :---: | :---: |
| ![Screenshot of the main page](img/mainpage.PNG) | ![Screenshot of failing validation](img/failedvalidation.PNG) | ![Screenshot of failing validation](img/passedvalidation.PNG) |

*Screenshot of Skill Sets*:

| Skill Set 10 | Skill Set 11 | Skill Set 12 |
| :---: | :---: | :---: |
| ![Screenshot of Skill Set 10 running](img/ArrayListDemo.PNG) | ![Screenshot of Skill Set 11 running](img/NestedStructures.PNG) | ![Screenshot of Skill Set 12 running](img/PersonDemo.PNG) |

#### Assignment Links:

*Local LIS4381 Web Application*:
[Link to local LIS4381 Web Application](http://localhost/repos/lis4381/ "My LIS4381 localhost Web Application")