# LIS4381 - Mobile Web Application Development

## Douglas Nandelstadt

### Assignment 5 Requirements:

*One part*:

1. Completed assignment 5 directory and files

	
#### README.md file should include the following items:

* Course title, my name, and the assignment requirements, as per A1
* Screenshot of assignment 5's index.php and add_petscore_process.php (that includes error.php)
* Screenshots of skill sets 13, 14, and 15
* Link to the local LIS4381 Web Application

#### Assignment Screenshots:

*Screenshots of Assignment 5's index.php and add_petscore_process.php (that includes error.php)*:

| index.php | add_petscore_process.php | 
| :---: | :---: | 
| ![Screenshot of index.php](img/index.PNG) | ![Screenshot of add_petscore_process.php](img/error.PNG) |

*Screenshot of Skill Set 13 (Extra Credit)*:

![Screenshot of Skill Set 13 running](img/EmployeeDemo.PNG)

*Screenshots of Skill Set 14*:

| Addition index.php | Addition process.php |
| :---: | :---: |
| ![Screenshot of index.php with addition option](img/simpleindex1.PNG) | ![Screenshot of process.php with addition option](img/simpleprocess1.PNG) |

| Division index.php | Division process.php |
| :---: | :---: |
| ![Screenshot of index.php with division option](img/simpleindex2.PNG) | ![Screenshot of process.php with division option](img/simpleprocess2.PNG) |

*Screenshots of Skill Set 15*:

| index.php | process.php |
| :---: | :---: |
| ![Screenshot of index.php](img/readindex.PNG) | ![Screenshot of process.php](img/readprocess.PNG) |

#### Assignment Links:

*Local LIS4381 Web Application*:
[Link to local LIS4381 Web Application](http://localhost/repos/lis4381/ "My LIS4381 localhost Web Application")