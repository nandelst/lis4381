> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4381 - Mobile Web Application Development

## Douglas Nandelstadt

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
	- Install AMPPS
	- Install Java Development Kit
	- Install Android Studio and create My First App
	- Provide screenshots of installations
	- Create Bitbucket repo
	- Complete Bitbucket Station Locations tutorial
	- Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Create Healthy Recipes Android app
	- Provide screenshots of completed app
	- Provide screenshots of skillsets 1, 2, and 3

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Provide screenshot of completed ERD
	- Create My Event Android app
	- Provide screenshots of completed app
	- Provide screenshots of skillsets 4, 5, and 6
	- Provide links to a3.mwb and a3.sql

4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Create local LIS4381 web app
	- Provide screenshots of local LIS4381 web app main page and of data validation
	- Provide screenshots of skillsets 10, 11, and 12

5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Complete assignment 5 directory and files
	- Provide screenshots of index.php and add_petscore_process.php
	- Provide screenshots of skillsets 13, 14, and 15

6. [P1 README.md](p1/README.md "My P1 README.md file")
	- Create My Business Card! app
	- Provide screenshots of completed app
	- Provide screenshots of skillsets 7, 8, and 9

7. [P2 README.md](p2/README.md "My P2 README.md file")
	- Complete project 2 directory and files
	- Provide screenshots of the carousel, index.php, edit_petstore.php, and delete_petstore.php
	- Provide screenshot of RSS feed page