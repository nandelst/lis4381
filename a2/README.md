# LIS4381 - Mobile Web Application Development

## Douglas Nandelstadt

### Assignment 2 Requirements:

*Three Parts:*

1. Healthy Recipes Mobile App
2. Chapter Questions
3. Bitbucket repo link

#### README.md file should include the following items:

* Screenshot of running Android Studio first and second user interface
* Screenshot of all 3 Java skill sets running
* Bitbucket repo link for this assignment

#### Assignment Screenshots:

*Screenshot of Android Studio Healthy Recipes*:

| App Main Page | Recipe Page |
| :---: | :---: |
| ![Screenshot of the app's main page](img/main.PNG) | ![Screenshot of the app's recipe page](img/recipe.PNG) |

*Screenshot of Skill Sets*:

| Skill Set 1 | Skill Set 2 | Skill Set 3 |
| :---: | :---: | :---: |
| ![Screenshot of Skill Set 1 running](img/EvenOrOdd.PNG) | ![Screenshot of Skill Set 2 running](img/LargestNumber.PNG) | ![Screenshot of Skill Set 3 running](img/ArraysAndLoops.PNG) |