<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio for LIS4381.">
		<meta name="author" content="Douglas Nandelstadt">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> In this assignment I created my second Android app, Healthy Recipes, which had two user interfaces.
				</p>
				
				<p class="text-justify">
					<strong>Requirements:</strong>
					<ol>
					<li>Screenshot of running application's first and second user interfaces</li>
					</ol>
				</p>

				<h4>First User Interface</h4>
				<img src="img/main.PNG" class="img-responsive center-block" alt="First user interface">

				<h4>Second User Interface</h4>
				<img src="img/recipe.PNG" class="img-responsive center-block" alt="Second user interface">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
