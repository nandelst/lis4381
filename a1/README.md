> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Douglas Nandelstadt

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links:

	a) this assignment, and
	
	b) the completed tutorial (bitbucketstationlocations).

#### README.md file should include the following items:

* Screenshot of AMPPS running http://localhost
* Screenshot of running JDK java Hello
* Screenshot of running Android Studio My First App
* git commands w/ short descriptions
* Bitbucket repo links:
	
	a) This assignment, and
	
	b) The completed tutorial repo above (bitbucketstationlocations).

#### Git commands w/short descriptions:

1. git init - Initializes a new Git repository.
2. git status - Lets you see the status of your repository such as new changes or tracked files.
3. git add - Stages a specific file or your directory for updates.
4. git commit - Saves your updates to the local repository.
5. git push - Uploads your local repository to a remote one.
6. git pull - Downloads a remote repository and updates your local repository.
7. git tag - Adds a reference number to a specific file to mark a version of it.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.PNG)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.PNG)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.PNG)

#### Tutorial Links:

*Assignment 1:* 
[A1 Link](https://bitbucket.org/nandelst/lis4381/ "Assignment 1 Repository")

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/nandelst/bitbucketstationlocations/ "Bitbucket Station Locations")
